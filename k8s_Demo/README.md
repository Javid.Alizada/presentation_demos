# Steps For Deploy Wordpress on Kubernetes

* Step 1 create Secret
  * kubectl create secret generic mysql --from-literal=password=myadmin
* Step 2 apply Persistent Volume Claim Definition for MySQL
  * kubectl apply -f mysql-volumeclaim.yaml
* Step 3 apply Deployment for MySQL (Deployment create our PODS with ReplicaSets and track they state create MySQL Pod )
  * kubectl apply -f mysql-deployment.yaml
* Step 4 apply MySQL service ( For create ClusterIP object for connection to MYSQL from other pods )
  * kubectl apply -f mysql-service.yaml
* Step 5 apply WordPress Persistent Volume Claim
  * kubectl apply -f wordpress-volumeclaim.yaml
* Step 6 apply WordPress Deployment
  * kubectl apply -f wordpress-deployment.yaml
* Step 7 apply WordPress Service ( this time we apply service type LoadBalancer and get public ip and LB from GC side for connection outside )
  * kubectl apply -f wordpress-service.yaml

